package com.swagger.demo.service;

import com.swagger.demo.dao.BookDao;
import com.swagger.demo.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookService {

    @Autowired
    BookDao bookDao;

    public String saveBook(Book book){
        bookDao.save(book);
        return "new Book was added with "+book.getBookId() ;
    }
    public Optional<Book> getBook(int bookId){
        return bookDao.findById(bookId);
    }
    public List<Book> removeBook(int bookId){
        bookDao.deleteById(bookId);
        return bookDao.findAll();
    }
}
