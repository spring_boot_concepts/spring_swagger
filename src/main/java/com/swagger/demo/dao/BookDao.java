package com.swagger.demo.dao;

import com.swagger.demo.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookDao  extends JpaRepository<Book,Integer> {
}
