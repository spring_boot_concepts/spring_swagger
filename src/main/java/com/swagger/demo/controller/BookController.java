package com.swagger.demo.controller;

import com.swagger.demo.model.Book;
import com.swagger.demo.service.BookService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/books")
@Api(value="Book Service",description = "My Book Store")
public class BookController {
     @Autowired
    BookService bookService;

     @PostMapping("/saveBook")
     @ApiOperation(value="save the book")
     public String saveBook(@RequestBody Book book){
        return bookService.saveBook(book);
     }

     @GetMapping("/getBook/{bookId}")
     @ApiOperation(value="get the book")
     public Optional<Book> getBook(@PathVariable int bookId){
         return bookService.getBook(bookId);
     }

     @DeleteMapping("/removeBook/{bookId}")
     @ApiOperation(value="remove the book")
     public List<Book> removeBook(@PathVariable int bookId){
         return bookService.removeBook(bookId);
     }
}
